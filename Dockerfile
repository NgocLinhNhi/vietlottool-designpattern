FROM maven:3.5.2-jdk-8

ADD /vietlottool-designpattern.jar app.jar

ENTRYPOINT exec java -jar app.jar