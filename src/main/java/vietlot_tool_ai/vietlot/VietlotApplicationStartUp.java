package vietlot_tool_ai.vietlot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import vietlot_tool_ai.vietlot.startup.VietLottStartUp;

@SpringBootApplication
public class VietlotApplicationStartUp {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(VietlotApplicationStartUp.class, args);
        VietLottStartUp.getInstance().start();
    }

}
