package vietlot_tool_ai.vietlot.abstract_factory;

import vietlot_tool_ai.vietlot.dao.IVietLottDAO;

//abstract factory design pattern
public abstract class VietLottAbstractFactory {

    public abstract IVietLottDAO createVietLott();

}
