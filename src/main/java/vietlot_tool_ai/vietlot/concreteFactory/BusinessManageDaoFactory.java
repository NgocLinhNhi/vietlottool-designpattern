package vietlot_tool_ai.vietlot.concreteFactory;

import vietlot_tool_ai.vietlot.abstract_factory.VietLottAbstractFactory;
import vietlot_tool_ai.vietlot.enums.VietLottType;

import java.util.HashMap;
import java.util.Map;

public class BusinessManageDaoFactory {
    private static BusinessManageDaoFactory INSTANCE;

    private Map<VietLottType, VietLottAbstractFactory> vietLottHandler;

    public static BusinessManageDaoFactory getInstance() {
        if (INSTANCE == null) INSTANCE = new BusinessManageDaoFactory();
        return INSTANCE;
    }

    private BusinessManageDaoFactory() {
        this.vietLottHandler = new HashMap<>();

        this.vietLottHandler.put(VietLottType.VIETLOTT645, new VietLott645ConcreteFactory());
        this.vietLottHandler.put(VietLottType.VIETLOTT655, new VietLott655ConcreteFactory());
    }

    public VietLottAbstractFactory getVietLottByType(VietLottType vietlottType) {
        return vietLottHandler.get(vietlottType);
    }
}
