package vietlot_tool_ai.vietlot.concreteFactory;

import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.handler.RequestHandler;
import vietlot_tool_ai.vietlot.handler.VietLott645InsertHandler;
import vietlot_tool_ai.vietlot.handler.VietLott655InsertHandler;

import java.util.HashMap;
import java.util.Map;

public class DataBaseHandleDaoFactory {
    private static DataBaseHandleDaoFactory INSTANCE;

    private Map<VietLottType, RequestHandler> vietLottHandler;

    public static DataBaseHandleDaoFactory getInstance() {
        if (INSTANCE == null) INSTANCE = new DataBaseHandleDaoFactory();
        return INSTANCE;
    }

    private DataBaseHandleDaoFactory() {
        this.vietLottHandler = new HashMap<>();

        this.vietLottHandler.put(VietLottType.VIETLOTT645, new VietLott645InsertHandler());
        this.vietLottHandler.put(VietLottType.VIETLOTT655, new VietLott655InsertHandler());
    }

    public RequestHandler getVietLottByType(VietLottType vietlottType) {
        return vietLottHandler.get(vietlottType);
    }
}
