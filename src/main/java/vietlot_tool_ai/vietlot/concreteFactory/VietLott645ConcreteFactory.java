package vietlot_tool_ai.vietlot.concreteFactory;

import vietlot_tool_ai.vietlot.abstract_factory.VietLottAbstractFactory;
import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.dao.IVietLottDAO;
import vietlot_tool_ai.vietlot.service.VietLottCommandImplements;
import vietlot_tool_ai.vietlot.service.BigDataHandlerService;


public class VietLott645ConcreteFactory extends VietLottAbstractFactory {
    @Override
    public IVietLottDAO createVietLott() {
        return new VietLottCommandImplements(new BigDataHandlerService(VietLottType.VIETLOTT645));
    }
}
