package vietlot_tool_ai.vietlot.concreteFactory;

import vietlot_tool_ai.vietlot.abstract_factory.VietLottAbstractFactory;
import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.dao.IVietLottDAO;
import vietlot_tool_ai.vietlot.service.BigDataHandlerService;
import vietlot_tool_ai.vietlot.service.VietLottCommandImplements;

public class VietLott655ConcreteFactory extends VietLottAbstractFactory {
    @Override
    public IVietLottDAO createVietLott() {
        return new VietLottCommandImplements(new BigDataHandlerService(VietLottType.VIETLOTT655));
    }

}
