package vietlot_tool_ai.vietlot.concreteFactory;

import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.handler.VietLott645ViewDataHandler;
import vietlot_tool_ai.vietlot.handler.VietLott655ViewDataHandler;
import vietlot_tool_ai.vietlot.handler.ViewDataRequestHandler;

import java.util.HashMap;
import java.util.Map;

public class VietLottResultManageFactory {
    private static VietLottResultManageFactory INSTANCE;

    private Map<VietLottType, ViewDataRequestHandler> vietlottViewNumberHandler;

    public static VietLottResultManageFactory getInstance() {
        if (INSTANCE == null) INSTANCE = new VietLottResultManageFactory();
        return INSTANCE;
    }

    private VietLottResultManageFactory() {
        this.vietlottViewNumberHandler = new HashMap<>();

        this.vietlottViewNumberHandler.put(VietLottType.VIETLOTT645, new VietLott645ViewDataHandler());
        this.vietlottViewNumberHandler.put(VietLottType.VIETLOTT655, new VietLott655ViewDataHandler());
    }

    public ViewDataRequestHandler getVietLottByType(VietLottType vietlottType) {
        return vietlottViewNumberHandler.get(vietlottType);
    }
}
