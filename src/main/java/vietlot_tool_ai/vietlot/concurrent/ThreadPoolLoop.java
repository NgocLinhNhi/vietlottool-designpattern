package vietlot_tool_ai.vietlot.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolLoop {

    private static ThreadPoolLoop INSTANCE = new ThreadPoolLoop();

    public static ThreadPoolLoop getInstance() {
        return INSTANCE;
    }

    public ExecutorService newExecutorService(int threadPoolSize) {
        return Executors.newFixedThreadPool(threadPoolSize);
    }

}
