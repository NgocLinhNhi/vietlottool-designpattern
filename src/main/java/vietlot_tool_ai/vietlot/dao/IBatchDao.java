package vietlot_tool_ai.vietlot.dao;

import vietlot_tool_ai.vietlot.enums.VietLottType;

import java.util.List;

public interface IBatchDao<T> {

    void truncateDataBase(VietLottType vietLottType) throws Exception;

    void insert(List<T> listData, String tableName) throws Exception;

    int[][] insertBatch(List<T> listData, int batchSize, String tableName) throws Exception;

    void insertExecutorBatch(List<T> listData, int batchSize, String tableName) throws Exception;

    void insertFuturesBatch(List<T> listData, int batchSize, String tableName) throws Exception;

    void insertThreadBatchPattern(List<T> listData, int batchSize, String tableName) throws Exception;
}
