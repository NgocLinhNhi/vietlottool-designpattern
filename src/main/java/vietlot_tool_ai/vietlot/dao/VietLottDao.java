package vietlot_tool_ai.vietlot.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.load_config.DBConfig;
import vietlot_tool_ai.vietlot.load_config.SQLQuerry;
import vietlot_tool_ai.vietlot.mapper.VietLott645Mapper;
import vietlot_tool_ai.vietlot.mapper.VietLott655Mapper;
import vietlot_tool_ai.vietlot.mapper.VietLottBigDataMapper;
import vietlot_tool_ai.vietlot.model.VietLott645Entity;
import vietlot_tool_ai.vietlot.prototype.DataBaseUtil;

import java.util.List;

import static vietlot_tool_ai.vietlot.constant.Constant.*;

public abstract class VietLottDao implements IVietLottDAO {

    @Override
    public Integer getNearestResultInWeek(VietLottType vietLottType, int numberInput, int dateDiff) throws Exception {
        //C1 : dùng jdbcTemplate (với trường hợp 1 parameter truyền cho nhiều vị trí trong câu sql )
        //nhược điểm nhìn câu sql append hơi rối mắt
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        jdbcTemplate.queryForObject(
                SQLQuerry.findNearestResultByDateDiff(DataBaseUtil.getTableName(vietLottType), numberInput, dateDiff).toString(),
                Integer.class);

        //C2 : dùng NamedParameterJdbcTemplate để set parameter trùng nhau số lượng nhiều trong 1 câu sql
        //ưu điểm nhìn câu sql không rối mắt
        //không truyền tham số như kiểu query được => sẽ thành truyền mấy chục tham số
        NamedParameterJdbcTemplate namedConnection = DBConfig.getInstance().getNamedConnection();
        return namedConnection.queryForObject(
                SQLQuerry.findNearestResultByDateDiff(DataBaseUtil.getTableName(vietLottType)).toString(),
                new MapSqlParameterSource()
                        .addValue("numberInput", numberInput)
                        .addValue("dateDiff", dateDiff),
                Integer.class);
    }

    //Number 1 Information
    //List<?> return nay ko bi warning khi complie voi List<VietLott645Entity>
    @Override
    @SuppressWarnings("unchecked") // bỏ qua checked compile VietLott645Mapper
    public List<?> selectNumber2ByNumber1(VietLottType vietLottType, int number_1) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_1, NUMBER_2),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_1);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber3ByNumber1(VietLottType vietLottType, int number_1) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_1, NUMBER_3),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_1);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber4ByNumber1(VietLottType vietLottType, int number_1) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_1, NUMBER_4),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_1);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber5ByNumber1(VietLottType vietLottType, int number_1) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_1, NUMBER_5),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_1);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber6ByNumber1(VietLottType vietLottType, int number_1) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_1, NUMBER_6),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_1);
    }

    //Number 2 information
    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber3ByNumber2(VietLottType vietLottType, int number_2) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_2, NUMBER_3),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_2);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber4ByNumber2(VietLottType vietLottType, int number_2) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_2, NUMBER_4),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_2);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber5ByNumber2(VietLottType vietLottType, int number_2) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_2, NUMBER_5),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_2);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber6ByNumber2(VietLottType vietLottType, int number_2) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_2, NUMBER_6),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_2);
    }

    //Number 3 information
    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber4ByNumber3(VietLottType vietLottType, int number_3) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_3, NUMBER_4),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_3);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber5ByNumber3(VietLottType vietLottType, int number_3) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_3, NUMBER_5),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_3);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber6ByNumber3(VietLottType vietLottType, int number_3) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_3, NUMBER_6),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_3);
    }

    //Number 4 information
    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber5ByNumber4(VietLottType vietLottType, int number_4) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_4, NUMBER_5),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_4);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber6ByNumber4(VietLottType vietLottType, int number_4) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_4, NUMBER_6),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_4);
    }

    //Number 5 information
    @Override
    @SuppressWarnings("unchecked")
    public List<?> selectNumber6ByNumber5(VietLottType vietLottType, int number_5) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType), NUMBER_5, NUMBER_6),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_5);
    }

    @Override
    public List<VietLott645Entity> loadAllData(VietLottType vietLottType) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.loadAllData(
                DataBaseUtil.getTableName(vietLottType)).toString(),
                new VietLottBigDataMapper());

    }

    @Override
    public List<VietLott645Entity> loadAllDataByLimitOffSet(VietLottType vietLottType, int limit, int offset) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.loadAllDataByLimitOffSet(
                DataBaseUtil.getTableName(vietLottType), limit, offset).toString(),
                new VietLottBigDataMapper());

    }
}
