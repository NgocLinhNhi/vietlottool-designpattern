package vietlot_tool_ai.vietlot.excel_import_handle;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import vietlot_tool_ai.vietlot.abstract_factory.VietLottAbstractFactory;
import vietlot_tool_ai.vietlot.concreteFactory.BusinessManageDaoFactory;
import vietlot_tool_ai.vietlot.dao.IVietLottDAO;
import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.excel_export_handle.ExcelWriteContentFile;
import vietlot_tool_ai.vietlot.model.VietLott645Entity;
import vietlot_tool_ai.vietlot.ulti.excel_apache_poi.ExcelParseFormat;
import vietlot_tool_ai.vietlot.ulti.excel_apache_poi.SimpleExcelWriter;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static vietlot_tool_ai.vietlot.constant.Constant.*;

public abstract class ExcelRequestHandler {

    private IVietLottDAO createDbProcessor(VietLottType vietLottType) {
        BusinessManageDaoFactory instance = BusinessManageDaoFactory.getInstance();
        VietLottAbstractFactory factory = instance.getVietLottByType(vietLottType);
        return factory.createVietLott();
    }

    protected List<VietLott645Entity> getListVietLott645(VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return dbProcessor.loadAllData(vietLottType);
    }

    void insertVietLott645ByExcelFile(List<VietLott645Entity> listData) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(VietLottType.VIETLOTT645);
        dbProcessor.insertDataVietLottByFileExcel(listData);
    }

    List<VietLott645Entity> handleImportExcelFile(String excelFilePath) throws Exception {
        List<VietLott645Entity> listData = new ArrayList<>();
        FileInputStream inputStream = new FileInputStream(excelFilePath);
        SimpleExcelWriter excelService = new SimpleExcelWriter(
                EXCEL_IMPORT_RESULT_PATH,
                EXCEL_IMPORT_RESULT_NAME);

        try (Workbook workbook = WorkbookFactory.create(inputStream)) {
            Iterator<Row> rowIterator = ExcelParseFormat.checkTypeOfWorkBook(workbook);
            Boolean validateResult = ExcelImportValidator.getInstance().validateRecords(rowIterator, listData);
            excelService.writeFileExcel(validateResult, workbook);
        }
        return listData;
    }

    protected void exportExcelFile(List<VietLott645Entity> listData) throws Exception {
        SimpleExcelWriter simpleExcelWriter = new SimpleExcelWriter(
                EXCEL_EXPORT_RESULT_PATH,
                EXCEL_EXPORT_FILE_NAME);

        try (XSSFWorkbook workbook = simpleExcelWriter.createXSSFWorkbook()) {
            XSSFSheet sheet = workbook.createSheet(SHEET_NAME);
            simpleExcelWriter.writeHeaderLine(sheet, VIETLOTT_645_HEADER);
            ExcelWriteContentFile.getInstance().writeDataLines(listData, workbook, sheet);
            simpleExcelWriter.writeFileExcel(true, workbook);
        }
    }

}
