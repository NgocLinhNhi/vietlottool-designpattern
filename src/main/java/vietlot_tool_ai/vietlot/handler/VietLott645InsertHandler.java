package vietlot_tool_ai.vietlot.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.interfaces.VietLottChainOfHandlers;
import vietlot_tool_ai.vietlot.model.VietLott645Entity;
import vietlot_tool_ai.vietlot.vietlott_chain_of_repository.VietLottSelectChainByServiceType;

import java.util.List;

//bên ngoài sử dụng factory method
public class VietLott645InsertHandler extends VietLott645InsertRequestHandler<VietLott645Entity> {

    private Logger logger = LoggerFactory.getLogger(VietLott645InsertHandler.class);

    //Validate for request here
    @Override
    protected void preHandle(List<VietLott645Entity> request) throws Exception {
        //Thật ra nên dùng 1 trong 2 thôi ( requestHandler or Chain Of Repository design pattern)
        // => requestHandler => xử lý message (JMS - activeMQ)
        // => Chain Of Repository design pattern => xử lý insert data / update => gồm nhiều steps validate + insert
        // => Chain Of Repository design pattern => xử lý deposit / withdraw
        logger.info("start validate  data for VietLott 6/45 request {}", request);
        //Bên trong sử dụng chains of repository design pattern
        VietLottSelectChainByServiceType instance = VietLottSelectChainByServiceType.getInstance();
        VietLottChainOfHandlers handlers = instance.newVietLottChainOfHandlers(VietLottType.VIETLOTT645);
        handlers.handle(VietLottSelectChainByServiceType.getInstance().newVietLottData(request, null));
    }

    @Override
    protected void doHandle(List<VietLott645Entity> request) throws Exception {
        insertDataVietLott645(request);
        logger.info("Insert data for VietLott645 has successfully with {} records", request.size());
    }

    @Override
    protected void postHandle(List<VietLott645Entity> request) {
        logger.info("end handle request for VietLott 6/45 !!!");
    }


}
