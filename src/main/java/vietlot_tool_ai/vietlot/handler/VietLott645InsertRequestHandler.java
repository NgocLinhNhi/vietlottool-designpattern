package vietlot_tool_ai.vietlot.handler;

import vietlot_tool_ai.vietlot.abstract_factory.VietLottAbstractFactory;
import vietlot_tool_ai.vietlot.concreteFactory.BusinessManageDaoFactory;
import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.dao.IVietLottDAO;
import vietlot_tool_ai.vietlot.model.VietLott645Entity;

import java.util.List;

abstract class VietLott645InsertRequestHandler<T> extends RequestHandler<T> {

    void insertDataVietLott645(List<VietLott645Entity> request) throws Exception {
        BusinessManageDaoFactory factory = BusinessManageDaoFactory.getInstance();
        VietLottAbstractFactory vietLottByType = factory.getVietLottByType(VietLottType.VIETLOTT645);
        IVietLottDAO vietLottDao = vietLottByType.createVietLott();
        vietLottDao.insertDataVietLott645(request);
    }

}
