package vietlot_tool_ai.vietlot.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.interfaces.VietLottChainOfHandlers;
import vietlot_tool_ai.vietlot.model.VietLott655Entity;
import vietlot_tool_ai.vietlot.vietlott_chain_of_repository.VietLottSelectChainByServiceType;

import java.util.List;

public class VietLott655InsertHandler extends VietLott655InsertRequestHandler<VietLott655Entity> {

    private Logger logger = LoggerFactory.getLogger(VietLott655InsertHandler.class);

    @Override
    protected void preHandle(List<VietLott655Entity> request) throws Exception {
        logger.info("start validate  data for VietLott 6/55 request {}", request);
        VietLottSelectChainByServiceType instance = VietLottSelectChainByServiceType.getInstance();
        VietLottChainOfHandlers handlers = instance.newVietLottChainOfHandlers(VietLottType.VIETLOTT655);
        handlers.handle(VietLottSelectChainByServiceType.getInstance().newVietLottData(null, request));
    }

    @Override
    protected void doHandle(List<VietLott655Entity> request) throws Exception {
        insertDataVietLott655(request);
        logger.info("Insert data for VietLott655 has successfully with {} records", request.size());
    }

    @Override
    protected void postHandle(List<VietLott655Entity> request) {
        logger.info("end handle request for VietLott 6/55 {} records ", request.size());
    }


}
