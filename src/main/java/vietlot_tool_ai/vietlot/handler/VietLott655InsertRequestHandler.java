package vietlot_tool_ai.vietlot.handler;

import vietlot_tool_ai.vietlot.abstract_factory.VietLottAbstractFactory;
import vietlot_tool_ai.vietlot.concreteFactory.BusinessManageDaoFactory;
import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.dao.IVietLottDAO;
import vietlot_tool_ai.vietlot.model.VietLott655Entity;

import java.util.List;

abstract class VietLott655InsertRequestHandler<T> extends RequestHandler<T> {

    void insertDataVietLott655(List<VietLott655Entity> request) throws Exception {
        BusinessManageDaoFactory factory = BusinessManageDaoFactory.getInstance();
        VietLottAbstractFactory vietLottByType = factory.getVietLottByType(VietLottType.VIETLOTT655);
        IVietLottDAO vietLottDao = vietLottByType.createVietLott();
        vietLottDao.insertDataVietLott655(request);
    }

}
