package vietlot_tool_ai.vietlot.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vietlot_tool_ai.vietlot.enums.VietLottType;

import static vietlot_tool_ai.vietlot.constant.Constant.*;

public class VietLott655ViewDataHandler extends VietLott655ViewDataRequestHandler {

    private Logger logger = LoggerFactory.getLogger(VietLott655ViewDataHandler.class);

    @Override
    protected void preHandle(int number, VietLottType vietLottType, String numberType) {
        logger.info("Begin export file view Number by Number {} - VietLott_655 !!!", numberType);
    }

    @Override
    protected void doHandle(int number, VietLottType vietLottType, String numberType) throws Exception {
        long timeStart = System.currentTimeMillis();
        logger.info("Time  Start export data for view VietLott_655 {} ", numberType);

        switch (numberType) {
            case NUMBER_1:
                executeViewData655ByNumber1(number, vietLottType);
                break;
            case NUMBER_2:
                executeViewData655ByNumber2(number, vietLottType);
                break;
            case NUMBER_3:
                executeViewData655ByNumber3(number, vietLottType);
                break;
            case NUMBER_4:
                executeViewData655ByNumber4(number, vietLottType);
                break;
            case NUMBER_5:
                executeViewData655ByNumber5(number, vietLottType);
                break;
        }

        long timeEnd = System.currentTimeMillis();
        logger.info("End time export data for view VietLott_655: {}", timeEnd);
        logger.info("Time End - Time Start: {} ms (1s=1000ms)", (timeEnd - timeStart));
    }

    @Override
    protected void postHandle(int number, VietLottType vietLottType, String numberType) {
        logger.info("End export file view Number by {} - VietLott_655 !!!", numberType);
    }

}
