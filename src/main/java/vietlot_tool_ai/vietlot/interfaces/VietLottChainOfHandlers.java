package vietlot_tool_ai.vietlot.interfaces;

import vietlot_tool_ai.vietlot.prototype.VietLottData;

public interface VietLottChainOfHandlers<T> {
    //Bride design pattern
    void addHandler(T handler);

    //VietLottData handle 1 class cho nó linh động hơn là chỉ List<VietLott645Entity>
    void handle(VietLottData data) throws Exception;

}
