package vietlot_tool_ai.vietlot.interfaces;

import vietlot_tool_ai.vietlot.prototype.VietLottData;

public interface VietLottHandler {
    void handle(VietLottData data) throws Exception;
}
