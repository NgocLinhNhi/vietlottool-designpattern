package vietlot_tool_ai.vietlot.load_config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExecutorConfig {
    public static int threadPoolSize = 10;
    public static int threadPoolSizeLv2 = 20;
}
