package vietlot_tool_ai.vietlot.service;

import vietlot_tool_ai.vietlot.dao.VietLottDao;
import vietlot_tool_ai.vietlot.model.VietLott645Entity;
import vietlot_tool_ai.vietlot.model.VietLott655Entity;

import java.util.List;

public class VietLottCommandImplements extends VietLottDao {

    private BigDataHandlerService commandService;

    public VietLottCommandImplements(BigDataHandlerService service) {
        this.commandService = service;
    }

    @Override
    public void truncateData() throws Exception {
        commandService.truncateDataCommand();
    }

    @Override
    public void insertDataVietLott645(List<VietLott645Entity> listData) throws Exception {
        commandService.insertDataCommand645(listData);
    }

    @Override
    public void insertDataVietLott655(List<VietLott655Entity> listData) throws Exception {
        commandService.insertDataCommand655(listData);
    }

    @Override
    public void insertDataVietLottByFileExcel(List<VietLott645Entity> listData) throws Exception {
        commandService.insertDataByFileExcel(listData);
    }

}
