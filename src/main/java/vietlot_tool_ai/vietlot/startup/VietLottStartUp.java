package vietlot_tool_ai.vietlot.startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vietlot_tool_ai.vietlot.concreteFactory.DataBaseHandleDaoFactory;
import vietlot_tool_ai.vietlot.concreteFactory.VietLottResultManageFactory;
import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.excel_export_handle.VietLott645ExportExcelHandler;
import vietlot_tool_ai.vietlot.excel_import_handle.VietLott645ImportExcelHandler;
import vietlot_tool_ai.vietlot.handler.RequestHandler;
import vietlot_tool_ai.vietlot.handler.ViewDataRequestHandler;
import vietlot_tool_ai.vietlot.interfaces.VietLottChainOfHandlers;
import vietlot_tool_ai.vietlot.load_config.PropertiesFileData;
import vietlot_tool_ai.vietlot.model.VietLott645Entity;
import vietlot_tool_ai.vietlot.model.VietLott655Entity;
import vietlot_tool_ai.vietlot.prototype.VietLottData;
import vietlot_tool_ai.vietlot.vietlott_chain_of_repository.VietLottSelectChainByServiceType;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import static vietlot_tool_ai.vietlot.constant.Constant.*;

public class VietLottStartUp {
    private static Logger logger = LoggerFactory.getLogger(VietLottStartUp.class);

    private static VietLottStartUp INSTANCE;

    public static VietLottStartUp getInstance() {
        if (INSTANCE == null) INSTANCE = new VietLottStartUp();
        return INSTANCE;
    }

    private List<VietLott645Entity> readFileCsv645Data() throws Exception {
        return PropertiesFileData.getInstance().getListData645FromCsvFile();
    }

    private List<VietLott655Entity> readFileCsv655Data() throws Exception {
        return PropertiesFileData.getInstance().getListData655FromCsvFile();
    }

    //Import Excel from Folder on server = APACHE POI
    private void importExcel() throws Exception {
        String filePathExcel = PropertiesFileData.getInstance().getFilePathExcelVietLott645();
        VietLott645ImportExcelHandler.getInstance().doHandle(filePathExcel);
    }

    //Export Excel from Database and save file on server = APACHE POI
    private void exportExcel() throws Exception {
        VietLott645ExportExcelHandler.getInstance().handleExportExcelFile(VietLottType.VIETLOTT645);
    }

    @SuppressWarnings("unchecked")
    //Import Csv from folder
    private void insertVietLottDataProcess(VietLottType vietLottType) throws Exception {
        DataBaseHandleDaoFactory factory = DataBaseHandleDaoFactory.getInstance();
        RequestHandler handlers = factory.getVietLottByType(vietLottType);
        handlers.handle(vietLottType == VietLottType.VIETLOTT645 ? readFileCsv645Data() : readFileCsv655Data());
    }

    //View data by CSV file
    private void exportViewDataByNumber(int number, VietLottType vietLottType, String numberType) {
        VietLottResultManageFactory factory = VietLottResultManageFactory.getInstance();
        ViewDataRequestHandler handlers = factory.getVietLottByType(vietLottType);
        handlers.handle(number, vietLottType, numberType);
    }

    //Export file Csv with Big Data Handler
    private void exportBigData(int number, VietLottType vietLottType, String numberType) {
        VietLottResultManageFactory factory = VietLottResultManageFactory.getInstance();
        ViewDataRequestHandler handlers = factory.getVietLottByType(vietLottType);
        handlers.handle(number, vietLottType, numberType);
    }

    //View data in number days by Csv file
    private void getDataIn30DaysNearest(VietLottType vietLottType, int dateDiff) throws Exception {
        //Chains of repository design pattern không lồng với template method pattern
        VietLottSelectChainByServiceType instance = VietLottSelectChainByServiceType.getInstance();
        VietLottChainOfHandlers handlers = instance.getDataByDateDiffChainOfHandlers(vietLottType);
        VietLottData vietLottData = instance.newVietLottDataByDateDiff(vietLottType, dateDiff);
        handlers.handle(vietLottData);
    }

    private static int getMenu() {
        int choose = 0;
        try {
            System.out.println("============MENU=========");
            System.out.println("1.insert Data for VietLott 6/45 or 6/55");
            System.out.println("2.Lấy kết quả 6 cặp số Vietlott 6/45 or 6/55 được chọn nhiều nhất trong ? ngày gần nhất");
            System.out.println("3.lấy cặp số VietLott 6/45 or 6/55 từ 2->6 được chọn nhiều nhất với số thứ 1 bạn muốn ");
            System.out.println("4.lấy cặp số VietLott 6/45 or 6/55 từ 3->6 được chọn nhiều nhất với số thứ 2 bạn muốn ");
            System.out.println("5.lấy cặp số VietLott 6/45 or 6/55 từ 4->6 được chọn nhiều nhất với số thứ 3 bạn muốn ");
            System.out.println("6.lấy cặp số VietLott 6/45 or 6/55 từ 5->6 được chọn nhiều nhất với số thứ 4 bạn muốn ");
            System.out.println("7.lấy cặp số VietLott 6/45or 6/55 thứ 6 được chọn nhiều nhất với số thứ 5 bạn muốn ");
            System.out.println("====================================================================== ");
            System.out.println("8.Export Big data (700k-1tr) - No use limit offset ");
            System.out.println("9.Export Big data (700k-1tr) - Use limit offset ");
            System.out.println("10.Import VietLott-645 by excel file (APACHE_POI) ");
            System.out.println("11.Export VietLott-645 to excel file (APACHE_POI) ");
            System.out.println("12.Exit");

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Please input your choose: ");
            choose = Integer.parseInt(br.readLine());
        } catch (Exception e) {
            System.out.println("Please input number: ");
        }

        return choose;
    }

    public void start() {
        int menu = 0;
        do {
            try {
                menu = getMenu();
                switch (menu) {
                    case 1:
                        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
                        int vietLottType = Integer.parseInt(br.readLine());
                        if (vietLottType == 1) {
                            getInstance().insertVietLottDataProcess(VietLottType.VIETLOTT645);
                        } else {
                            getInstance().insertVietLottDataProcess(VietLottType.VIETLOTT655);
                        }
                        break;
                    case 2:
                        br = new BufferedReader(new InputStreamReader(System.in));
                        System.out.println("Please input Date Diff ");
                        int dateDiff = Integer.parseInt(br.readLine());
                        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
                        vietLottType = Integer.parseInt(br.readLine());
                        if (vietLottType == 1) {
                            getDataIn30DaysNearest(VietLottType.VIETLOTT645, dateDiff);
                        } else {
                            getDataIn30DaysNearest(VietLottType.VIETLOTT655, dateDiff);
                        }
    
                        break;
                    case 3:
                        br = new BufferedReader(new InputStreamReader(System.in));
                        System.out.println("Please input number 1");
                        int number_1 = Integer.parseInt(br.readLine());
                        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
                        vietLottType = Integer.parseInt(br.readLine());
                        if (vietLottType == 1) {
                            exportViewDataByNumber(number_1, VietLottType.VIETLOTT645, NUMBER_1);
                        } else {
                            exportViewDataByNumber(number_1, VietLottType.VIETLOTT655, NUMBER_1);
                        }
                        break;
                    case 4:
                        br = new BufferedReader(new InputStreamReader(System.in));
                        System.out.println("Please input number 2");
                        int number_2 = Integer.parseInt(br.readLine());
                        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
                        vietLottType = Integer.parseInt(br.readLine());
                        if (vietLottType == 1) {
                            exportViewDataByNumber(number_2, VietLottType.VIETLOTT645, NUMBER_2);
                        } else {
                            exportViewDataByNumber(number_2, VietLottType.VIETLOTT655, NUMBER_2);
                        }
                        break;
                    case 5:
                        br = new BufferedReader(new InputStreamReader(System.in));
                        System.out.println("Please input number 3");
                        int number_3 = Integer.parseInt(br.readLine());
                        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
                        vietLottType = Integer.parseInt(br.readLine());
                        if (vietLottType == 1) {
                            exportViewDataByNumber(number_3, VietLottType.VIETLOTT645, NUMBER_3);
                        } else {
                            exportViewDataByNumber(number_3, VietLottType.VIETLOTT655, NUMBER_3);
                        }
                        break;
                    case 6:
                        br = new BufferedReader(new InputStreamReader(System.in));
                        System.out.println("Please input number 4");
                        int number_4 = Integer.parseInt(br.readLine());
                        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
                        vietLottType = Integer.parseInt(br.readLine());
                        if (vietLottType == 1) {
                            exportViewDataByNumber(number_4, VietLottType.VIETLOTT645, NUMBER_4);
                        } else {
                            exportViewDataByNumber(number_4, VietLottType.VIETLOTT655, NUMBER_4);
                        }
                        break;
                    case 7:
                        br = new BufferedReader(new InputStreamReader(System.in));
                        System.out.println("Please input number 5");
                        int number_5 = Integer.parseInt(br.readLine());
                        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
                        vietLottType = Integer.parseInt(br.readLine());
                        if (vietLottType == 1) {
                            exportViewDataByNumber(number_5, VietLottType.VIETLOTT645, NUMBER_5);
                        } else {
                            exportViewDataByNumber(number_5, VietLottType.VIETLOTT655, NUMBER_5);
                        }
                        break;
                    case 8:
                        exportBigData(1, VietLottType.VIETLOTT645, NUMBER_6);
                        break;
                    case 9:
                        exportBigData(2, VietLottType.VIETLOTT645, NUMBER_7);
                        break;
                    case 10:
                        //Import file Excel by APACHE POI dùng HSSFWorkbook / XSSFWorkbook
                        importExcel();
                        break;
                    case 11:
                        //Export file Excel by APACHE POI dùng HSSFWorkbook / XSSFWorkbook
                        exportExcel();
                        break;
                    case 12:
                        System.exit(0);
                        break;
                }
            } catch (Exception e) {
                logger.info("Exception {}", e.getMessage());
            }
        } while (menu != 13);
    }

    public static void main(String[] args) {
        //Nên chạy từ mục 2 trước lấy cặp số ra nhiều nhất trong ? ngày gần nhất => để chọn ra số đầu tiên được chọn nhiều nhất
        //Nên chọn từ số 1 quy ra số 2 => lấy số 2 chạy quy ra số 3 => lấy số 3 đó chạy quy ra số 4
        //đừng nên chọn số 1 quy ra gần hết hoặc tất
        //Step cuối : note coi từ file tổng kết theo ngày -> các số lớn (4-5-6) có hay ra không để chốt lại lần cuối
        //=> chon 1 cap số đẹp theo đuổi 1 thời gian. ..

        //Note : hoặc chạy mục 3 rồi từ số 1 lấy luôn 6 cặp số cũng đc
        getInstance().start();
    }
}

