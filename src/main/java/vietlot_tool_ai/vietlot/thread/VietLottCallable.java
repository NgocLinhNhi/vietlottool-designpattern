package vietlot_tool_ai.vietlot.thread;

import lombok.Getter;
import lombok.Setter;
import vietlot_tool_ai.vietlot.model.VietLott645Entity;

import java.util.List;
import java.util.concurrent.Callable;

@Getter
@Setter
public class VietLottCallable implements Callable<String> {

    private List<VietLott645Entity> listData;
    private int batchSize;
    private String tableName;

    VietLottCallable(List<VietLott645Entity> listData,
                     int batchSize,
                     String tableName) {
        this.listData = listData;
        this.batchSize = batchSize;
        this.tableName = tableName;
    }

    @Override
    //Do implement Callable nên method Call sẽ tự được gọi khi tạo mới class
    public String call() throws Exception {
        return handlerProduct(this.listData, this.batchSize, this.tableName);
    }

    private String handlerProduct(List<VietLott645Entity> listData,
                                  int batchSize,
                                  String tableName) throws Exception {
        return VietLotCallableHandler.getInstance().handlerProductCallable(listData, batchSize, tableName);
    }
}
