package vietlot_tool_ai.vietlot.ulti;

import vietlot_tool_ai.vietlot.model.VietLott645Entity;
import vietlot_tool_ai.vietlot.model.VietLott655Entity;
import vietlot_tool_ai.vietlot.ulti.csv.CsvReader;
import vietlot_tool_ai.vietlot.ulti.csv.SimpleCsvReader;
import vietlot_tool_ai.vietlot.ulti.date.DateUtil;

import java.util.List;

import static vietlot_tool_ai.vietlot.constant.DateConstant.PATTERN_YYMMDD_BLANK;

public class CsvReaderUtil {
    private static CsvReaderUtil INSTANCE;

    public static CsvReaderUtil getInstance() {
        if (INSTANCE == null) INSTANCE = new CsvReaderUtil();
        return INSTANCE;
    }

    public List<VietLott645Entity> readDataVietLott645(String filePath) {
        //Builder design pattern
        CsvReader csvReader = new SimpleCsvReader()
                //bỏ line đầu -> line tiêu đề
                .skip(1)
                .filePath(filePath);

        return csvReader.readToObjects(values -> new VietLott645Entity()
                .setPeriod(Integer.valueOf(values[0].trim()))
                .setNumber1(Integer.valueOf(values[1].trim()))
                .setNumber2(Integer.valueOf(values[2].trim()))
                .setNumber3(Integer.valueOf(values[3].trim()))
                .setNumber4(Integer.valueOf(values[4].trim()))
                .setNumber5(Integer.valueOf(values[5].trim()))
                .setNumber6(Integer.valueOf(values[6].trim()))
                .setDateOfResult(DateUtil.toDate(values[7].trim(), PATTERN_YYMMDD_BLANK))
                .build());
    }

    public List<VietLott655Entity> readDataVietLott655(String filePath) {
        CsvReader csvReader = new SimpleCsvReader()
                .skip(1)
                .filePath(filePath);

        return csvReader.readToObjects(values -> new VietLott655Entity()
                .setPeriod(Integer.valueOf(values[0].trim()))
                .setNumber1(Integer.valueOf(values[1].trim()))
                .setNumber2(Integer.valueOf(values[2].trim()))
                .setNumber3(Integer.valueOf(values[3].trim()))
                .setNumber4(Integer.valueOf(values[4].trim()))
                .setNumber5(Integer.valueOf(values[5].trim()))
                .setNumber6(Integer.valueOf(values[6].trim()))
                .setDateOfResult(DateUtil.toDate(values[7].trim(), PATTERN_YYMMDD_BLANK))
                .build());
    }
}
