package vietlot_tool_ai.vietlot.ulti;

import java.io.File;

import static vietlot_tool_ai.vietlot.constant.Constant.*;

public class ExportDataUtil {

    public static StringBuilder buildHeader(String byNumber, String buildNumber) {
        StringBuilder buildHeader = new StringBuilder();

        buildHeader.append(byNumber);
        buildHeader.append(CSV_SEPARATOR);
        buildHeader.append(buildNumber);
        buildHeader.append(CSV_SEPARATOR);
        buildHeader.append(COUNT);
        buildHeader.append(CSV_SEPARATOR);

        return buildHeader;
    }

    public static StringBuilder buildHeaderBigData() {
        StringBuilder buildHeader = new StringBuilder();

        buildHeader.append(SEQ_ID);
        buildHeader.append(CSV_SEPARATOR);
        buildHeader.append(NUMBER_1);
        buildHeader.append(CSV_SEPARATOR);
        buildHeader.append(NUMBER_2);
        buildHeader.append(CSV_SEPARATOR);
        buildHeader.append(NUMBER_3);
        buildHeader.append(CSV_SEPARATOR);
        buildHeader.append(NUMBER_4);
        buildHeader.append(CSV_SEPARATOR);
        buildHeader.append(NUMBER_5);
        buildHeader.append(CSV_SEPARATOR);
        buildHeader.append(NUMBER_6);
        buildHeader.append(CSV_SEPARATOR);
        buildHeader.append(DATE_RESULT);
        buildHeader.append(CSV_SEPARATOR);

        return buildHeader;
    }

    public static String getCsvFileName(String fileName) {
        return CSV_EXPORT_FILE_PATH + File.separator + fileName;
    }
}
