package vietlot_tool_ai.vietlot.ulti.csv;

public class CsvWriteException extends RuntimeException {

    public CsvWriteException(String msg, Exception e) {
        super(msg, e);
    }

}
