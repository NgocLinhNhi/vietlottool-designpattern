package vietlot_tool_ai.vietlot.ulti.io;

import java.io.File;

public interface FileFetcher {

    File getFile(String filePath);

}
