package vietlot_tool_ai.vietlot.ulti.io;

import java.io.InputStream;

public interface InputStreamFetcher {

    InputStream getInputStream(String filePath);

}
