package vietlot_tool_ai.vietlot.vietlott_chain_of_repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vietlot_tool_ai.vietlot.interfaces.VietLottHandler;
import vietlot_tool_ai.vietlot.model.VietLott645Entity;
import vietlot_tool_ai.vietlot.prototype.VietLottData;
import vietlot_tool_ai.vietlot.ulti.StringUtil;

import java.util.List;

public class VietLott645Step1ValidateRequestHandler implements VietLottHandler {

    private Logger logger = LoggerFactory.getLogger(VietLott645Step1ValidateRequestHandler.class);

    @Override
    public void handle(VietLottData request) throws Exception {
        List<VietLott645Entity> listVietLott645 = request.getListVietLott645();
        logger.info("start validate  data for VietLott645 request has {} records", listVietLott645.size());
        String result = validatData(listVietLott645);
        if (!StringUtil.isEmpty(result)) {
            throw new Exception("Validate File has Error");
        }
    }

    private String validatData(List<VietLott645Entity> request) {
        logger.info("Validate data {}", request);
        return null;
    }
}
