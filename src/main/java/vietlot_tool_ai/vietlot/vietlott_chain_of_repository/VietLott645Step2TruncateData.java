package vietlot_tool_ai.vietlot.vietlott_chain_of_repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vietlot_tool_ai.vietlot.abstract_factory.VietLottAbstractFactory;
import vietlot_tool_ai.vietlot.concreteFactory.BusinessManageDaoFactory;
import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.interfaces.VietLottHandler;
import vietlot_tool_ai.vietlot.prototype.VietLottData;

public class VietLott645Step2TruncateData implements VietLottHandler {
    private Logger logger = LoggerFactory.getLogger(VietLott645Step2TruncateData.class);

    @Override
    public void handle(VietLottData data) throws Exception {
        logger.info("Begin truncate table VietLott645 in Database");
        VietLottAbstractFactory vietLottByType = BusinessManageDaoFactory.getInstance().getVietLottByType(VietLottType.VIETLOTT645);
        vietLottByType.createVietLott().truncateData();
        logger.info("Truncate table VietLott_645 successfully");
    }

}
