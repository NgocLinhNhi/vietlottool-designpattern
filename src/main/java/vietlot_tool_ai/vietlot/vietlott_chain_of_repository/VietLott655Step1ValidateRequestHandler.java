package vietlot_tool_ai.vietlot.vietlott_chain_of_repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vietlot_tool_ai.vietlot.interfaces.VietLottHandler;
import vietlot_tool_ai.vietlot.model.VietLott655Entity;
import vietlot_tool_ai.vietlot.prototype.VietLottData;
import vietlot_tool_ai.vietlot.ulti.StringUtil;

import java.util.List;

import static vietlot_tool_ai.vietlot.constant.Constant.VIETLOTT_655_VALIDATE;

public class VietLott655Step1ValidateRequestHandler implements VietLottHandler {

    private Logger logger = LoggerFactory.getLogger(VietLott655Step1ValidateRequestHandler.class);

    @SuppressWarnings("unchecked")
    @Override
    public void handle(VietLottData data) throws Exception {
        //C1 : lấy thẳng data được get/set
        List<VietLott655Entity> listVietLott655 = data.getListVietLott655();
        //C2: lấy = stepData
        List<VietLott655Entity> stepData = data.getStepData(VIETLOTT_655_VALIDATE);
        logger.info("Data input {}", stepData.size());

        logger.info("start validate  data for VietLott655 request has {} records", listVietLott655.size());
        String result = validateData(listVietLott655);

        //ví dụ có bước xào nấu lại list data List<VietLott645Entity> sau validate
        //step 1 add listData đã validate vào Map với key = VIETLOTT_655_VALIDATE or key khác
        data.addStepData(VIETLOTT_655_VALIDATE, listVietLott655);
        if (!StringUtil.isEmpty(result)) {
            throw new Exception("Validate File has Error");
        }
    }

    private String validateData(List<VietLott655Entity> request) {
        logger.info("Validate data {}", request);
        return null;
    }
}
