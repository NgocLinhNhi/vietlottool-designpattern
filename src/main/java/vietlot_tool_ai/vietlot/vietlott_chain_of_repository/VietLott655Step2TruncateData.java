package vietlot_tool_ai.vietlot.vietlott_chain_of_repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vietlot_tool_ai.vietlot.abstract_factory.VietLottAbstractFactory;
import vietlot_tool_ai.vietlot.concreteFactory.BusinessManageDaoFactory;
import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.interfaces.VietLottHandler;
import vietlot_tool_ai.vietlot.model.VietLott655Entity;
import vietlot_tool_ai.vietlot.prototype.VietLottData;

import java.util.List;

import static vietlot_tool_ai.vietlot.constant.Constant.VIETLOTT_655_VALIDATE;

public class VietLott655Step2TruncateData implements VietLottHandler {
    private Logger logger = LoggerFactory.getLogger(VietLott655Step2TruncateData.class);

    @SuppressWarnings("unchecked")
    @Override
    public void handle(VietLottData data) throws Exception {
        //Ví dụ steps 2 là insert data chẳng hạn get lại List<T> data được xào nấu từ steps 1
        List<VietLott655Entity> lstData = data.getStepData(VIETLOTT_655_VALIDATE);
        logger.info("List Data after validate {}", lstData);

        logger.info("Begin truncate table VietLott645 in Database");
        VietLottAbstractFactory vietLottByType = BusinessManageDaoFactory.getInstance().getVietLottByType(VietLottType.VIETLOTT655);
        vietLottByType.createVietLott().truncateData();
        logger.info("Truncate table VietLott_655 successfully");
    }

}
