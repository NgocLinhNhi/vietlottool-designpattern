package vietlot_tool_ai.vietlot.vietlott_chain_of_repository;

import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.interfaces.VietLottChainOfHandlers;
import vietlot_tool_ai.vietlot.model.VietLott645Entity;
import vietlot_tool_ai.vietlot.model.VietLott655Entity;
import vietlot_tool_ai.vietlot.prototype.VietLottData;

import java.util.List;

import static vietlot_tool_ai.vietlot.constant.Constant.VIETLOTT_645_VALIDATE;
import static vietlot_tool_ai.vietlot.constant.Constant.VIETLOTT_655_VALIDATE;

public class VietLottSelectChainByServiceType {

    public static VietLottSelectChainByServiceType getInstance() {
        return new VietLottSelectChainByServiceType();
    }

    //Chain of Repository design pattern
    public VietLottChainOfHandlers newVietLottChainOfHandlers(VietLottType serviceType) {
        VietLottSimpleChainOfHandlers answer = VietLottSimpleChainOfHandlers.getInstance();
        if (serviceType == VietLottType.VIETLOTT645)
            answer.addVietLott645Handlers();
        else
            answer.addVietLott655Handlers();
        return answer;
    }

    public VietLottChainOfHandlers getDataByDateDiffChainOfHandlers(VietLottType serviceType) {
        VietLottSimpleChainOfHandlers answer = VietLottSimpleChainOfHandlers.getInstance();
        if (serviceType == VietLottType.VIETLOTT645)
            answer.addData645ByDateDiffHandlers();
        else
            answer.addData655ByDateDiffHandlers();
        return answer;
    }

    public VietLottData newVietLottData(List<VietLott645Entity> vietlott645, List<VietLott655Entity> vietlott655) {
        //C1 : add = get/set
        VietLottData vietLottData = new VietLottData(vietlott645, vietlott655);
        //C2: add vào stepData hoặc inputData
        vietLottData.addStepData(VIETLOTT_655_VALIDATE, vietlott655);
        vietLottData.addStepData(VIETLOTT_645_VALIDATE, vietlott645);
        return vietLottData;
    }

    public VietLottData newVietLottDataByDateDiff(VietLottType vietLottType, int dateDiff) {
        return new VietLottData(vietLottType, dateDiff);
    }
}
