package vietlot_tool_ai.vietlot.vietlott_chain_of_repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vietlot_tool_ai.vietlot.abstract_factory.VietLottAbstractFactory;
import vietlot_tool_ai.vietlot.concreteFactory.BusinessManageDaoFactory;
import vietlot_tool_ai.vietlot.enums.VietLottType;
import vietlot_tool_ai.vietlot.dao.IVietLottDAO;
import vietlot_tool_ai.vietlot.interfaces.VietLottHandler;
import vietlot_tool_ai.vietlot.prototype.VietLottData;

import java.util.HashMap;
import java.util.Map;

import static vietlot_tool_ai.vietlot.constant.Constant.*;

public class VietLottStep1GetDataNearestByDateDiff implements VietLottHandler {

    private Logger logger = LoggerFactory.getLogger(VietLottStep1GetDataNearestByDateDiff.class);
    private Map<Integer, Integer> dataResultByDateDif = new HashMap<>();

    @Override
    public void handle(VietLottData request) throws Exception {
        long timeStart = System.currentTimeMillis();
        logger.info("Start step 1 process get data VIETLOTT nearest by date Diff : {} ", timeStart);

        int dateDiff = request.getDateDiff();
        VietLottType vietLottType = request.getVietLottType();
        getDataNearestByDateDiff(vietLottType, dateDiff, request);

        //45 câu lệnh sql search thống kê( trong 807 data) chỉ trong 1219 ms ...
        long timeEnd = System.currentTimeMillis();
        logger.info("End step 1 process get data VIETLOTT nearest by date Diff : {}", timeEnd);
        logger.info("Time End - Time Start: {} ms (1s=1000ms)", (timeEnd - timeStart));
    }


    private void getDataNearestByDateDiff(VietLottType vietLottType, int dateDiff, VietLottData data) throws Exception {
        VietLottAbstractFactory factory = BusinessManageDaoFactory.getInstance().getVietLottByType(vietLottType);
        IVietLottDAO vietLott = factory.createVietLott();
        getDataByDateDiff(vietLottType == VietLottType.VIETLOTT655 ? NUMBER_655 : NUMBER_645, vietLottType, dateDiff, vietLott);

        if (vietLottType == VietLottType.VIETLOTT645) {
            data.addInputDataDateDiff(VIETLOTT_645_BY_DATE_DIFF, dataResultByDateDif);
        } else {
            data.addInputDataDateDiff(VIETLOTT_655_BY_DATE_DIFF, dataResultByDateDif);
        }
    }

    private void getDataByDateDiff(int numberInput,
                                   VietLottType vietLottType,
                                   int dateDiff,
                                   IVietLottDAO vietLott) throws Exception {
        for (int number = 1; number <= numberInput; number++) {
            Integer nearestResultInWeek = vietLott.getNearestResultInWeek(vietLottType, number, dateDiff);
            dataResultByDateDif.put(number, nearestResultInWeek);
        }
    }

}
