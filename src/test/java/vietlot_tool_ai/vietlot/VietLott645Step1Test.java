package vietlot_tool_ai.vietlot;

import org.junit.jupiter.api.Test;
import vietlot_tool_ai.vietlot.model.VietLott645Entity;
import vietlot_tool_ai.vietlot.prototype.VietLottData;
import vietlot_tool_ai.vietlot.vietlott_chain_of_repository.VietLott645Step1ValidateRequestHandler;

import java.util.ArrayList;
import java.util.List;

class VietLott645Step1Test {
    @Test
    void validateStringUtil() throws Exception {
        VietLott645Step1ValidateRequestHandler step1 = new VietLott645Step1ValidateRequestHandler();
        step1.handle(createVietLottData());
    }

    private VietLottData createVietLottData() {
        return new VietLottData(createVietLott645Object(), null);
    }

    private List<VietLott645Entity> createVietLott645Object() {
        List<VietLott645Entity> listResult = new ArrayList<>();
        VietLott645Entity vietLott645Entity = new VietLott645Entity();
        vietLott645Entity.setNumber1(50);
        listResult.add(vietLott645Entity);
        return listResult;
    }
}
