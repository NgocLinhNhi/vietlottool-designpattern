package vietlot_tool_ai.vietlot;

import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class VietlotApplicationTests {

    //đây là cách Unit test SpringJPA với java application

//    @Autowired
//    private DataProcessRepository repository;
//
//    @Test
//    public void testSaveAndFindAll() {
//        repository.truncateTable();
//    }
}